/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.govermentadminrole;

import business.NationalGoverment;
import business.enterprise.Enterprise;
import business.monitorData.MonitorData;
import business.organization.MonitorManagerOrganization;
import business.organization.Organization;
import business.stategoverment.StateGoverment;
import business.useraccount.UserAccount;
import business.workqueue.MonitorEmployeeWorkRequest;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author yanyu
 */
public class PrintAllThingsNational extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private NationalGoverment national;
    private StateGoverment state;
    private Enterprise enterprise;
    private UserAccount account;
    private Organization organization;


    /**
     * Creates new form PrintAllThingsNational
     */
    public PrintAllThingsNational(JPanel userProcessContainer,
            NationalGoverment national
            //StateGoverment state,
            //Enterprise enterprise,
            //UserAccount account,
            //Organization organization
            ){
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.national = national;
        //this.state = state;
        //this.enterprise = enterprise;
        //this.organization = organization;
        //this.account = account;
        populateStateComboBox();
        populateEnterpriseComboBox();
    }

    public void populateStateComboBox() {
        StateComboBox.removeAllItems();
        StateComboBox1.removeAllItems();
        StateComboBox.addItem("All");
        StateComboBox1.addItem("All");
        for(StateGoverment s: national.getStateList()){
            StateComboBox.addItem(s);
            StateComboBox1.addItem(s);
        }
        
    }

    public void populateEnterpriseComboBox(){
        EnterpriseComboBox.removeAllItems();
        EnterpriseComboBox.addItem("All");
        for(StateGoverment s: national.getStateList()){
            for(Enterprise e:s.getEnterpriseDirectory().getEnterpriseList()){
                EnterpriseComboBox.addItem(e);
            }
        }
        
    }
    
    public void populateTable(){
        DefaultTableModel model = (DefaultTableModel) PrintTable_1.getModel();

        model.setRowCount(0);
        if (StateComboBox.getSelectedItem().equals("All") && EnterpriseComboBox.getSelectedItem().equals("All")) {
            for (StateGoverment s : national.getStateList()) {
                for (Enterprise e : s.getEnterpriseDirectory().getEnterpriseList()) {
                    for (Organization o : e.getOrganizationDirectory().getOrganizationList()) {
                        Object[] row = new Object[4];
                        row[0] = s.getName();
                        row[1] = e.getEnterpriseType();
                        row[2] = e.getName();
                        row[3] = o.getName();

                        model.addRow(row);
                    }
                }
            }
        }
        else if ((!StateComboBox.getSelectedItem().equals("All")) && EnterpriseComboBox.getSelectedItem().equals("All")) {
            for (StateGoverment s : national.getStateList()) {
                if (s.equals(StateComboBox.getSelectedItem())) {
                    //s.equals(StateComboBox.getSelectedItem());
                    for (Enterprise e : s.getEnterpriseDirectory().getEnterpriseList()) {
                        for (Organization o : e.getOrganizationDirectory().getOrganizationList()) {
                            Object[] row = new Object[4];
                            row[0] = s.getName();
                            row[1] = e.getEnterpriseType();
                            row[2] = e.getName();
                            row[3] = o.getName();

                            model.addRow(row);
                        }
                    }
                }
            }
        }
        else if ((!StateComboBox.getSelectedItem().equals("All")) && (!EnterpriseComboBox.getSelectedItem().equals("All"))) {

            for (StateGoverment s : national.getStateList()) {
                if (s.equals(StateComboBox.getSelectedItem())) {
                    //s.equals(StateComboBox.getSelectedItem());
                    for (Enterprise e : s.getEnterpriseDirectory().getEnterpriseList()) {
                        if (e.equals(EnterpriseComboBox.getSelectedItem())) {

                            for (Organization o : e.getOrganizationDirectory().getOrganizationList()) {
                                Object[] row = new Object[4];
                                row[0] = s.getName();
                                row[1] = e.getEnterpriseType();
                                row[2] = e.getName();
                                row[3] = o.getName();

                                model.addRow(row);
                            }
                        }
                    }
                }
            }
        }else if(StateComboBox.getSelectedItem().equals("All") && (!EnterpriseComboBox.getSelectedItem().equals("All"))){
            for (StateGoverment s : national.getStateList()) {
                //if (s.equals(StateComboBox.getSelectedItem())) {
                    //s.equals(StateComboBox.getSelectedItem());
                    for (Enterprise e : s.getEnterpriseDirectory().getEnterpriseList()) {
                        if (e.equals(EnterpriseComboBox.getSelectedItem())) {

                            for (Organization o : e.getOrganizationDirectory().getOrganizationList()) {
                                Object[] row = new Object[4];
                                row[0] = s.getName();
                                row[1] = e.getEnterpriseType();
                                row[2] = e.getName();
                                row[3] = o.getName();

                                model.addRow(row);
                            }
                        }
                    }
                //}
            }            
        }
        
        
    }
    
    public void populateMonitorDataTable(){
        DefaultTableModel model = (DefaultTableModel) MonitorDataTable.getModel();

        model.setRowCount(0);
        if (StateComboBox1.getSelectedItem().equals("All")) {
            for (StateGoverment s : national.getStateList()) {
                for (Enterprise e : s.getEnterpriseDirectory().getEnterpriseList()) {
                    for (Organization o : e.getOrganizationDirectory().getOrganizationList()) {
                        if (o instanceof MonitorManagerOrganization) {
                            for (MonitorData monitorData : o.getMonitorDataDirectory().getMonitorDataList()) {
                                Object[] row = new Object[6];
                                row[0] = s.getName();
                                row[1] = o.getName();
                                row[2] = monitorData.getPolutionIndex();
                                row[3] = monitorData.getYear();
                                row[4] = monitorData.getMonth();
                                row[5] = monitorData.getDay();

                                model.addRow(row);
                            }

                        }
                    }
                }
            }
        }
        else if(!StateComboBox1.getSelectedItem().equals("All")){
            for (StateGoverment s : national.getStateList()) {
                if(s.equals(StateComboBox1.getSelectedItem())){
                    s.equals(StateComboBox1.getSelectedItem());
                for (Enterprise e : s.getEnterpriseDirectory().getEnterpriseList()) {
                    for (Organization o : e.getOrganizationDirectory().getOrganizationList()) {
                        if (o instanceof MonitorManagerOrganization) {
                            for (MonitorData monitorData : o.getMonitorDataDirectory().getMonitorDataList()) {
                                Object[] row = new Object[6];
                                row[0] = s.getName();
                                row[1] = o.getName();
                                row[2] = monitorData.getPolutionIndex();
                                row[3] = monitorData.getYear();
                                row[4] = monitorData.getMonth();
                                row[5] = monitorData.getDay();

                                model.addRow(row);
                            }

                        }
                    }
                }
            }}
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        StateComboBox = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        PrintTable_1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        EnterpriseComboBox = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        MonitorDataTable = new javax.swing.JTable();
        CheckMonitorData = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        StateComboBox1 = new javax.swing.JComboBox();
        jButton2 = new javax.swing.JButton();

        jLabel1.setText("States:");

        StateComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        PrintTable_1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "State", "Enterprise Type", "Enterprise Name", "Organization Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(PrintTable_1);

        jLabel2.setText("Enterprise:");

        EnterpriseComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jButton1.setText("Check");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        MonitorDataTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "State", "MonitorName", "AQI", "Year", "Month", "Day"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(MonitorDataTable);

        CheckMonitorData.setText("Check");
        CheckMonitorData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckMonitorDataActionPerformed(evt);
            }
        });

        jLabel3.setText("States:");

        StateComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jButton2.setText("Print Line Chart");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 646, Short.MAX_VALUE)
                            .addComponent(jScrollPane2)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jButton1))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(StateComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(37, 37, 37)
                                        .addComponent(jLabel2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(EnterpriseComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(StateComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(CheckMonitorData, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(StateComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(EnterpriseComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(StateComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CheckMonitorData)
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addContainerGap(213, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        populateTable();
        

        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void CheckMonitorDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckMonitorDataActionPerformed
        // TODO add your handling code here:
        populateMonitorDataTable();
    }//GEN-LAST:event_CheckMonitorDataActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        PrintLineChartJPanel panel = new PrintLineChartJPanel(userProcessContainer, national);
                userProcessContainer.add("PrintLineChartJPanel",panel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton CheckMonitorData;
    private javax.swing.JComboBox EnterpriseComboBox;
    private javax.swing.JTable MonitorDataTable;
    private javax.swing.JTable PrintTable_1;
    private javax.swing.JComboBox StateComboBox;
    private javax.swing.JComboBox StateComboBox1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables
}
