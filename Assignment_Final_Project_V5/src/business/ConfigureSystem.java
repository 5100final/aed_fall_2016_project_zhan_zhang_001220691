/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;


import static business.enterprise.Enterprise.EnterpriseType.MedicalDepartment;
import business.enterprise.EnvironmentDepartment;
import business.enterprise.Factory;
import business.enterprise.MedicalDepartment;
import business.enterprise.MonitoringStation;
import business.enterprise.ResearchInstitute;
import business.monitorData.MonitorData;
import business.monitorData.MonitorDataDirectory;
import business.organization.EnvironmentManagerOrganization;
import business.organization.FactoryManagerOrganization;
import business.organization.MDManagerOrganization;
import business.organization.MonitorEmployeeOrganization;
import business.organization.MonitorManagerOrganization;
import business.organization.ResearcherOrganization;
import business.patientData.PatientDataDirectory;
import business.role.AdminRole;
import business.role.EnvironmentManagerRole;
import business.role.FactoryManagerRole;
import business.role.GovermentAdminRole;
import business.role.MDManagerRole;
import business.role.MonitorEmployeeRole;
import business.role.MonitorManagerRole;
import business.role.ResearcherRole;
import business.role.StateAdminRole;
import business.stategoverment.StateGoverment;
import business.useraccount.UserAccount;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author zml
 */
public class ConfigureSystem {
    public  static NationalGoverment configure(){
        
        NationalGoverment system = NationalGoverment.getInstance();
        UserAccount ua = system.getUserAccountDirectory().createUserAccount("sysadmin", "sysadmin", new GovermentAdminRole());
    
        //MA
        MonitorDataDirectory directoryMA = new MonitorDataDirectory();
        StateGoverment stateMA = system.createAndAddState("MA");
        stateMA.setName("Massachusetts");
        UserAccount s1 = stateMA.getUserAccountDirectory().createUserAccount("ma", "ma", new StateAdminRole());
        ResearchInstitute r1 = new ResearchInstitute("Research Institute");
            r1.getUserAccountDirectory().createUserAccount("r1", "r1", new AdminRole());
            r1.setStateName("Massachusetts");
        MedicalDepartment md1 = new MedicalDepartment("Medical Department");
            md1.getUserAccountDirectory().createUserAccount("md1", "md1", new AdminRole());
            md1.setStateName("Massachusetts");
        EnvironmentDepartment ed1 = new EnvironmentDepartment("Environment Department");
            ed1.getUserAccountDirectory().createUserAccount("ed1", "ed1", new AdminRole());
            ed1.setStateName("Massachusetts");
        Factory f1 = new Factory("MAFactory A");
            f1.getUserAccountDirectory().createUserAccount("f1", "f1", new AdminRole());
        Factory f2 = new Factory("MAFactory B");
            f2.getUserAccountDirectory().createUserAccount("f2", "f2", new AdminRole());
        Factory f3 = new Factory("MAFactory C");
            f3.getUserAccountDirectory().createUserAccount("f3", "f3", new AdminRole());
        Factory f4 = new Factory("MAFactory D");
            f4.getUserAccountDirectory().createUserAccount("f4", "f4", new AdminRole());
        Factory f5 = new Factory("MAFactory E");
            f5.getUserAccountDirectory().createUserAccount("f5", "f5", new AdminRole());
        MonitoringStation ms1 = new MonitoringStation("MAMonitor A");
            ms1.getUserAccountDirectory().createUserAccount("ms1", "ms1", new AdminRole());
        MonitoringStation ms2 = new MonitoringStation("MAMonitor B");
            ms2.getUserAccountDirectory().createUserAccount("ms2", "ms2", new AdminRole());
        MonitoringStation ms3 = new MonitoringStation("MAMonitor C");
            ms3.getUserAccountDirectory().createUserAccount("ms3", "ms3", new AdminRole());
        MonitoringStation ms4 = new MonitoringStation("MAMonitor D");
            ms4.getUserAccountDirectory().createUserAccount("ms4", "ms4", new AdminRole());
        MonitoringStation ms5 = new MonitoringStation("MAMonitor E");
            ms5.getUserAccountDirectory().createUserAccount("ms5", "ms5", new AdminRole());
        
        ResearcherOrganization researcherOrganization = new ResearcherOrganization();
            r1.getOrganizationDirectory().getOrganizationList().add(researcherOrganization);
                researcherOrganization.getUserAccountDirectory().createUserAccount("ro1", "ro1", new ResearcherRole());
        MDManagerOrganization mdManagerOrganization = new MDManagerOrganization();
            md1.getOrganizationDirectory().getOrganizationList().add(mdManagerOrganization);
                mdManagerOrganization.getUserAccountDirectory().createUserAccount("mdo1", "mdo1",new MDManagerRole());
        EnvironmentManagerOrganization environmentManagerOrganization = new EnvironmentManagerOrganization();
            ed1.getOrganizationDirectory().getOrganizationList().add(environmentManagerOrganization);
                environmentManagerOrganization.getUserAccountDirectory().createUserAccount("edo1", "edo1",new EnvironmentManagerRole());
        FactoryManagerOrganization factoryManagerOrganizationn = new FactoryManagerOrganization();
            f1.getOrganizationDirectory().getOrganizationList().add(factoryManagerOrganizationn);
                factoryManagerOrganizationn.getUserAccountDirectory().createUserAccount("fo1", "fo1",new FactoryManagerRole());
        MonitorManagerOrganization monitorManagerOrganization = new MonitorManagerOrganization();
            ms1.getOrganizationDirectory().getOrganizationList().add(monitorManagerOrganization);
                monitorManagerOrganization.getUserAccountDirectory().createUserAccount("mso1", "mso1",new MonitorManagerRole());
        MonitorEmployeeOrganization monitorEmployeeOrganization = new MonitorEmployeeOrganization();
            ms1.getOrganizationDirectory().getOrganizationList().add(monitorEmployeeOrganization);
                monitorEmployeeOrganization.getUserAccountDirectory().createUserAccount("mse1", "mse1",new MonitorEmployeeRole());

        stateMA.getEnterpriseDirectory().getEnterpriseList().add(r1);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(md1);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(ed1);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(f1);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(f2);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(f3);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(f4);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(f5);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(ms1);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(ms2);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(ms3);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(ms4);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(ms5);
        //MA-DATA
        ArrayList<MonitorData> monitorDataList = new ArrayList<>();
        for(int i=1;i<=12;i++){
            for(int j=1;j<=30;j++){
                Random random = new Random();
                int ka = random.nextInt(30)+20;
                int kb = random.nextInt(30)+20;
                int kc = random.nextInt(30)+20;
                int kd = random.nextInt(30)+20;
                int ke = random.nextInt(30)+20;
                
                
                directoryMA.CreateMonitorData("MAMonitor A",ka,null,2016,i,j);
                directoryMA.CreateMonitorData("MAMonitor B",kb,null,2016,i,j);
                directoryMA.CreateMonitorData("MAMonitor C",kc,null,2016,i,j);
                directoryMA.CreateMonitorData("MAMonitor D",kd,null,2016,i,j);
                directoryMA.CreateMonitorData("MAMonitor E",ke,null,2016,i,j);
                //monitorManagerOrganization.setMonitorDataDirectory(directoryMA);
            }
        }
        //monitorDataList = directoryMA.getMonitorDataList();
        
       monitorManagerOrganization.setMonitorDataDirectory(directoryMA);
        //Washington
        //6-104
        //CA
        MonitorDataDirectory directoryCA = new MonitorDataDirectory();
        StateGoverment stateCA = system.createAndAddState("CA");
        stateCA.setName("California");
        UserAccount s2 = stateCA.getUserAccountDirectory().createUserAccount("ca", "ca", new StateAdminRole());
        ResearchInstitute r2 = new ResearchInstitute("Research Institute");
            r2.getUserAccountDirectory().createUserAccount("r2", "r2", new AdminRole());
        MedicalDepartment md2 = new MedicalDepartment("Medical Department");
            md2.getUserAccountDirectory().createUserAccount("md2", "md2", new AdminRole());
        EnvironmentDepartment ed2 = new EnvironmentDepartment("Environment Department");
            ed2.getUserAccountDirectory().createUserAccount("ed2", "ed2", new AdminRole());
        Factory ff1 = new Factory("CAFactory A");
            ff1.getUserAccountDirectory().createUserAccount("ff1", "ff1", new AdminRole());
        Factory ff2 = new Factory("CAFactory B");
            ff2.getUserAccountDirectory().createUserAccount("ff2", "ff2", new AdminRole());
        Factory ff3 = new Factory("CAFactory C");
            ff3.getUserAccountDirectory().createUserAccount("ff3", "ff3", new AdminRole());
        Factory ff4 = new Factory("CAFactory D");
            ff4.getUserAccountDirectory().createUserAccount("ff4", "ff4", new AdminRole());
        Factory ff5 = new Factory("CAFactory E");
            ff5.getUserAccountDirectory().createUserAccount("ff5", "ff5", new AdminRole());
        MonitoringStation mss1 = new MonitoringStation("CAMonitor A");
            mss1.getUserAccountDirectory().createUserAccount("mss1", "mss1", new AdminRole());
        MonitoringStation mss2 = new MonitoringStation("CAMonitor B");
            mss2.getUserAccountDirectory().createUserAccount("mss1", "mss2", new AdminRole());
        MonitoringStation mss3 = new MonitoringStation("CAMonitor C");
            mss3.getUserAccountDirectory().createUserAccount("mss1", "mss3", new AdminRole());
        MonitoringStation mss4 = new MonitoringStation("CAMonitor D");
            mss4.getUserAccountDirectory().createUserAccount("mss1", "mss4", new AdminRole());
        MonitoringStation mss5 = new MonitoringStation("CAMonitor E");
            mss5.getUserAccountDirectory().createUserAccount("mss1", "mss5", new AdminRole());
            
        ResearcherOrganization researcherOrganization2 = new ResearcherOrganization();
            r2.getOrganizationDirectory().getOrganizationList().add(researcherOrganization2);
                researcherOrganization2.getUserAccountDirectory().createUserAccount("ro2", "ro2", new ResearcherRole());
        MDManagerOrganization mdManagerOrganization2 = new MDManagerOrganization();
            md2.getOrganizationDirectory().getOrganizationList().add(mdManagerOrganization2);
                mdManagerOrganization2.getUserAccountDirectory().createUserAccount("mdo2", "mdo2",new MDManagerRole());
        EnvironmentManagerOrganization environmentManagerOrganization2 = new EnvironmentManagerOrganization();
            ed2.getOrganizationDirectory().getOrganizationList().add(environmentManagerOrganization2);
                environmentManagerOrganization2.getUserAccountDirectory().createUserAccount("edo2", "edo2",new EnvironmentManagerRole());
        FactoryManagerOrganization factoryManagerOrganizationn2 = new FactoryManagerOrganization();
            ff2.getOrganizationDirectory().getOrganizationList().add(factoryManagerOrganizationn2);
                factoryManagerOrganizationn2.getUserAccountDirectory().createUserAccount("fo2", "fo2",new FactoryManagerRole());
        MonitorManagerOrganization monitorManagerOrganization2 = new MonitorManagerOrganization();
            mss2.getOrganizationDirectory().getOrganizationList().add(monitorManagerOrganization2);
                monitorManagerOrganization2.getUserAccountDirectory().createUserAccount("mso2", "mso2",new MonitorManagerRole());
        MonitorEmployeeOrganization monitorEmployeeOrganization2 = new MonitorEmployeeOrganization();
            mss2.getOrganizationDirectory().getOrganizationList().add(monitorEmployeeOrganization2);
                monitorEmployeeOrganization2.getUserAccountDirectory().createUserAccount("mse2", "mse2",new MonitorEmployeeRole());
                
        stateCA.getEnterpriseDirectory().getEnterpriseList().add(r2);
        stateCA.getEnterpriseDirectory().getEnterpriseList().add(md2);
        stateCA.getEnterpriseDirectory().getEnterpriseList().add(ed2);
        stateCA.getEnterpriseDirectory().getEnterpriseList().add(ff1);
        stateCA.getEnterpriseDirectory().getEnterpriseList().add(ff2);
        stateCA.getEnterpriseDirectory().getEnterpriseList().add(ff3);
        stateCA.getEnterpriseDirectory().getEnterpriseList().add(ff4);
        stateCA.getEnterpriseDirectory().getEnterpriseList().add(ff5);
        stateCA.getEnterpriseDirectory().getEnterpriseList().add(mss1);
        stateCA.getEnterpriseDirectory().getEnterpriseList().add(mss2);
        stateCA.getEnterpriseDirectory().getEnterpriseList().add(mss3);
        stateCA.getEnterpriseDirectory().getEnterpriseList().add(mss4);
        stateCA.getEnterpriseDirectory().getEnterpriseList().add(mss5);
        //CA-DATA
        for(int i=1;i<=12;i++){
            for(int j=1;j<=30;j++){
                Random random = new Random();
                int ka = random.nextInt(50)+60;
                int kb = random.nextInt(50)+60;
                int kc = random.nextInt(50)+60;
                int kd = random.nextInt(50)+60;
                int ke = random.nextInt(50)+60;
                directoryCA.CreateMonitorData("CAMonitor A",ka,null,2016,i,j);
                directoryCA.CreateMonitorData("CAMonitor B",kb,null,2016,i,j);
                directoryCA.CreateMonitorData("CAMonitor C",kc,null,2016,i,j);
                directoryCA.CreateMonitorData("CAMonitor D",kd,null,2016,i,j);
                directoryCA.CreateMonitorData("CAMonitor E",ke,null,2016,i,j);
            }
        }
        monitorManagerOrganization2.setMonitorDataDirectory(directoryCA);
       
        
        //WASHINTON
        MonitorDataDirectory directoryWA = new MonitorDataDirectory();
        StateGoverment stateWA = system.createAndAddState("WA");
        stateWA.setName("Washington");
        UserAccount s3 = stateWA.getUserAccountDirectory().createUserAccount("wa", "wa", new StateAdminRole());
        ResearchInstitute r3 = new ResearchInstitute("Research Institute");
            r3.getUserAccountDirectory().createUserAccount("r3", "r3", new AdminRole());
        MedicalDepartment md3 = new MedicalDepartment("Medical Department");
            md3.getUserAccountDirectory().createUserAccount("md3", "md3", new AdminRole());
        EnvironmentDepartment ed3 = new EnvironmentDepartment("Environment Department");
            ed3.getUserAccountDirectory().createUserAccount("ed3", "ed3", new AdminRole());
        Factory fff1 = new Factory("WAFactory A");
            fff1.getUserAccountDirectory().createUserAccount("fff1", "fff1", new AdminRole());
        Factory fff2 = new Factory("WAFactory B");
            fff2.getUserAccountDirectory().createUserAccount("fff2", "fff2", new AdminRole());
        Factory fff3 = new Factory("WAFactory C");
            fff3.getUserAccountDirectory().createUserAccount("fff3", "fff3", new AdminRole());
        Factory fff4 = new Factory("WAFactory D");
            fff4.getUserAccountDirectory().createUserAccount("fff4", "fff4", new AdminRole());
        Factory fff5 = new Factory("WAFactory E");
            fff5.getUserAccountDirectory().createUserAccount("fff5", "fff5", new AdminRole());
        MonitoringStation msss1 = new MonitoringStation("WAMonitor A");
            msss1.getUserAccountDirectory().createUserAccount("msss1", "msss1", new AdminRole());
        MonitoringStation msss2 = new MonitoringStation("WAMonitor B");
            msss2.getUserAccountDirectory().createUserAccount("msss2", "msss2", new AdminRole());
        MonitoringStation msss3 = new MonitoringStation("WAMonitor C");
            msss3.getUserAccountDirectory().createUserAccount("msss3", "msss3", new AdminRole());
        MonitoringStation msss4 = new MonitoringStation("WAMonitor D");
            msss4.getUserAccountDirectory().createUserAccount("msss4", "msss4", new AdminRole());
        MonitoringStation msss5 = new MonitoringStation("WAMonitor E");
            msss5.getUserAccountDirectory().createUserAccount("msss5", "msss5", new AdminRole());
        
        ResearcherOrganization researcherOrganization3 = new ResearcherOrganization();
            r3.getOrganizationDirectory().getOrganizationList().add(researcherOrganization3);
                researcherOrganization3.getUserAccountDirectory().createUserAccount("ro3", "ro3", new ResearcherRole());
        MDManagerOrganization mdManagerOrganization3 = new MDManagerOrganization();
            md3.getOrganizationDirectory().getOrganizationList().add(mdManagerOrganization3);
                mdManagerOrganization3.getUserAccountDirectory().createUserAccount("mdo3", "mdo3",new MDManagerRole());
        EnvironmentManagerOrganization environmentManagerOrganization3 = new EnvironmentManagerOrganization();
            ed3.getOrganizationDirectory().getOrganizationList().add(environmentManagerOrganization3);
                environmentManagerOrganization3.getUserAccountDirectory().createUserAccount("edo3", "edo3",new EnvironmentManagerRole());
        FactoryManagerOrganization factoryManagerOrganizationn3 = new FactoryManagerOrganization();
            fff3.getOrganizationDirectory().getOrganizationList().add(factoryManagerOrganizationn3);
                factoryManagerOrganizationn3.getUserAccountDirectory().createUserAccount("fo3", "fo3",new FactoryManagerRole());
        MonitorManagerOrganization monitorManagerOrganization3 = new MonitorManagerOrganization();
            msss3.getOrganizationDirectory().getOrganizationList().add(monitorManagerOrganization3);
                monitorManagerOrganization3.getUserAccountDirectory().createUserAccount("mso3", "mso3",new MonitorManagerRole());
        MonitorEmployeeOrganization monitorEmployeeOrganization3 = new MonitorEmployeeOrganization();
            msss3.getOrganizationDirectory().getOrganizationList().add(monitorEmployeeOrganization3);
                monitorEmployeeOrganization3.getUserAccountDirectory().createUserAccount("mse3", "mse3",new MonitorEmployeeRole());

        stateWA.getEnterpriseDirectory().getEnterpriseList().add(r3);
        stateWA.getEnterpriseDirectory().getEnterpriseList().add(md3);
        stateWA.getEnterpriseDirectory().getEnterpriseList().add(ed3);
        stateWA.getEnterpriseDirectory().getEnterpriseList().add(fff1);
        stateWA.getEnterpriseDirectory().getEnterpriseList().add(fff2);
        stateWA.getEnterpriseDirectory().getEnterpriseList().add(fff3);
        stateWA.getEnterpriseDirectory().getEnterpriseList().add(fff4);
        stateWA.getEnterpriseDirectory().getEnterpriseList().add(fff5);
        stateWA.getEnterpriseDirectory().getEnterpriseList().add(msss1);
        stateWA.getEnterpriseDirectory().getEnterpriseList().add(msss2);
        stateWA.getEnterpriseDirectory().getEnterpriseList().add(msss3);
        stateWA.getEnterpriseDirectory().getEnterpriseList().add(msss4);
        stateWA.getEnterpriseDirectory().getEnterpriseList().add(msss5);
        //MA-DATA
        for(int i=1;i<=12;i++){
            for(int j=1;j<=30;j++){
                Random random = new Random();
                int ka = random.nextInt(35)+45;
                int kb = random.nextInt(35)+45;
                int kc = random.nextInt(35)+45;
                int kd = random.nextInt(35)+45;
                int ke = random.nextInt(35)+45;
                directoryWA.CreateMonitorData("WAMonitor A",ka,null,2016,i,j);
                directoryWA.CreateMonitorData("WAMonitor B",kb,null,2016,i,j);
                directoryWA.CreateMonitorData("WAMonitor C",kc,null,2016,i,j);
                directoryWA.CreateMonitorData("WAMonitor D",kd,null,2016,i,j);
                directoryWA.CreateMonitorData("WAMonitor E",ke,null,2016,i,j);
            }
        }
        monitorManagerOrganization3.setMonitorDataDirectory(directoryWA);
        
        
        PatientDataDirectory directory = new PatientDataDirectory();
        directory.CreatePatientData("Massachusetts", 2016, 01, 1214, 265, 336, 58, 167);
        directory.CreatePatientData("Massachusetts", 2016, 02, 814, 199, 312, 64, 185);
        directory.CreatePatientData("Massachusetts", 2016, 03, 1089, 213, 256, 56, 200);
        directory.CreatePatientData("Massachusetts", 2016, 04, 2134, 237, 215, 54, 115);
        directory.CreatePatientData("Massachusetts", 2016, 05, 834, 190, 314, 68, 124);
        directory.CreatePatientData("Massachusetts", 2016, 06, 1024, 173, 301, 78, 156);
        directory.CreatePatientData("Massachusetts", 2016, 07, 987, 157, 189, 57, 208);
        directory.CreatePatientData("Massachusetts", 2016, 8, 956, 182, 225, 82, 214);
        directory.CreatePatientData("Massachusetts", 2016, 9, 978, 175, 217, 85, 224);
        directory.CreatePatientData("Massachusetts", 2016, 10, 890, 189, 347, 67, 167);
        directory.CreatePatientData("Massachusetts", 2016, 11, 865, 208, 236, 74, 158);
        directory.CreatePatientData("Massachusetts", 2016, 12, 1278, 215, 214, 77, 153);
        directory.CreatePatientData("Massachusetts", 2015, 01, 1089, 203, 388, 53, 167);
        directory.CreatePatientData("Massachusetts", 2015, 02, 814, 199, 312, 57, 185);
        directory.CreatePatientData("Massachusetts", 2015, 03, 1089, 213, 256, 56, 200);
        directory.CreatePatientData("Massachusetts", 2015, 04, 2134, 237, 215, 54, 115);
        directory.CreatePatientData("Massachusetts", 2015, 05, 834, 130, 314, 68, 124);
        directory.CreatePatientData("Massachusetts", 2015, 06, 1024, 173, 301, 78, 156);
        directory.CreatePatientData("Massachusetts", 2015, 07, 987, 157, 189, 53, 208);
        directory.CreatePatientData("Massachusetts", 2015, 8, 956, 192, 225, 82, 214);
        directory.CreatePatientData("Massachusetts", 2015, 9, 978, 185, 217, 88, 224);
        directory.CreatePatientData("Massachusetts", 2015, 10, 890, 189, 347, 67, 167);
        directory.CreatePatientData("Massachusetts", 2015, 11, 865, 208, 236, 65, 158);
        directory.CreatePatientData("Massachusetts", 2015, 12, 1278, 215, 214, 77, 153);
        stateMA.setPatientDataDirectory(directory);
        
        return system;
    }
}
