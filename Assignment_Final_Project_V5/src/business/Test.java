/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import business.enterprise.EnvironmentDepartment;
import business.enterprise.Factory;
import business.enterprise.MedicalDepartment;
import business.enterprise.MonitoringStation;
import business.enterprise.ResearchInstitute;
import business.monitorData.MonitorData;
import business.monitorData.MonitorDataDirectory;
import business.organization.EnvironmentManagerOrganization;
import business.organization.FactoryManagerOrganization;
import business.organization.MDManagerOrganization;
import business.organization.MonitorEmployeeOrganization;
import business.organization.MonitorManagerOrganization;
import business.organization.ResearcherOrganization;
import business.role.AdminRole;
import business.role.EnvironmentManagerRole;
import business.role.FactoryManagerRole;
import business.role.GovermentAdminRole;
import business.role.MDManagerRole;
import business.role.MonitorEmployeeRole;
import business.role.MonitorManagerRole;
import business.role.ResearcherRole;
import business.role.StateAdminRole;
import business.stategoverment.StateGoverment;
import business.useraccount.UserAccount;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author yanyu
 */
public class Test {
    public static  void main(String args[]){
        NationalGoverment system = NationalGoverment.getInstance();
        UserAccount ua = system.getUserAccountDirectory().createUserAccount("sysadmin", "sysadmin", new GovermentAdminRole());
    
        //MA
        MonitorDataDirectory directoryMA = new MonitorDataDirectory();
        StateGoverment stateMA = system.createAndAddState("MA");
        stateMA.setName("Massachusetts");
        UserAccount s1 = stateMA.getUserAccountDirectory().createUserAccount("ma", "ma", new StateAdminRole());
        ResearchInstitute r1 = new ResearchInstitute("Research Institute");
            r1.getUserAccountDirectory().createUserAccount("r1", "r1", new AdminRole());
        MedicalDepartment md1 = new MedicalDepartment("Medical Department");
            md1.getUserAccountDirectory().createUserAccount("md1", "md1", new AdminRole());
        EnvironmentDepartment ed1 = new EnvironmentDepartment("Environment Department");
            ed1.getUserAccountDirectory().createUserAccount("ed1", "ed1", new AdminRole());
        Factory f1 = new Factory("MAFactory A");
            f1.getUserAccountDirectory().createUserAccount("f1", "f1", new AdminRole());
        Factory f2 = new Factory("MAFactory B");
            f2.getUserAccountDirectory().createUserAccount("f2", "f2", new AdminRole());
        Factory f3 = new Factory("MAFactory C");
            f3.getUserAccountDirectory().createUserAccount("f3", "f3", new AdminRole());
        Factory f4 = new Factory("MAFactory D");
            f4.getUserAccountDirectory().createUserAccount("f4", "f4", new AdminRole());
        Factory f5 = new Factory("MAFactory E");
            f5.getUserAccountDirectory().createUserAccount("f5", "f5", new AdminRole());
        MonitoringStation ms1 = new MonitoringStation("MAMonitor A");
            ms1.getUserAccountDirectory().createUserAccount("ms1", "ms1", new AdminRole());
        MonitoringStation ms2 = new MonitoringStation("MAMonitor B");
            ms2.getUserAccountDirectory().createUserAccount("ms2", "ms2", new AdminRole());
        MonitoringStation ms3 = new MonitoringStation("MAMonitor C");
            ms3.getUserAccountDirectory().createUserAccount("ms3", "ms3", new AdminRole());
        MonitoringStation ms4 = new MonitoringStation("MAMonitor D");
            ms4.getUserAccountDirectory().createUserAccount("ms4", "ms4", new AdminRole());
        MonitoringStation ms5 = new MonitoringStation("MAMonitor E");
            ms5.getUserAccountDirectory().createUserAccount("ms5", "ms5", new AdminRole());
        
        ResearcherOrganization researcherOrganization = new ResearcherOrganization();
            r1.getOrganizationDirectory().getOrganizationList().add(researcherOrganization);
                researcherOrganization.getUserAccountDirectory().createUserAccount("ro1", "ro1", new ResearcherRole());
        MDManagerOrganization mdManagerOrganization = new MDManagerOrganization();
            md1.getOrganizationDirectory().getOrganizationList().add(mdManagerOrganization);
                mdManagerOrganization.getUserAccountDirectory().createUserAccount("mdo1", "mdo1",new MDManagerRole());
        EnvironmentManagerOrganization environmentManagerOrganization = new EnvironmentManagerOrganization();
            ed1.getOrganizationDirectory().getOrganizationList().add(environmentManagerOrganization);
                environmentManagerOrganization.getUserAccountDirectory().createUserAccount("edo1", "edo1",new EnvironmentManagerRole());
        FactoryManagerOrganization factoryManagerOrganizationn = new FactoryManagerOrganization();
            f1.getOrganizationDirectory().getOrganizationList().add(factoryManagerOrganizationn);
                factoryManagerOrganizationn.getUserAccountDirectory().createUserAccount("fo1", "fo1",new FactoryManagerRole());
        MonitorManagerOrganization monitorManagerOrganization = new MonitorManagerOrganization();
            ms1.getOrganizationDirectory().getOrganizationList().add(monitorManagerOrganization);
                monitorManagerOrganization.getUserAccountDirectory().createUserAccount("mso1", "mso1",new MonitorManagerRole());
        MonitorEmployeeOrganization monitorEmployeeOrganization = new MonitorEmployeeOrganization();
            ms1.getOrganizationDirectory().getOrganizationList().add(monitorEmployeeOrganization);
                monitorEmployeeOrganization.getUserAccountDirectory().createUserAccount("mse1", "mse1",new MonitorEmployeeRole());

        stateMA.getEnterpriseDirectory().getEnterpriseList().add(r1);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(md1);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(ed1);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(f1);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(f2);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(f3);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(f4);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(f5);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(ms1);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(ms2);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(ms3);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(ms4);
        stateMA.getEnterpriseDirectory().getEnterpriseList().add(ms5);
        //MA-DATA
        ArrayList<MonitorData> monitorDataList = new ArrayList<>();
        for(int i=1;i<=12;i++){
            for(int j=1;j<=30;j++){
                Random random = new Random();
                int ka = random.nextInt(62)+6;
                int kb = random.nextInt(62)+6;
                int kc = random.nextInt(62)+6;
                int kd = random.nextInt(62)+6;
                int ke = random.nextInt(62)+6;
                
                
                directoryMA.CreateMonitorData("MAMonitor A",ka,null,2016,i,j);
                directoryMA.CreateMonitorData("MAMonitor B",kb,null,2016,i,j);
                directoryMA.CreateMonitorData("MAMonitor C",kc,null,2016,i,j);
                directoryMA.CreateMonitorData("MAMonitor D",kd,null,2016,i,j);
                directoryMA.CreateMonitorData("MAMonitor E",ke,null,2016,i,j);
                //monitorManagerOrganization.setMonitorDataDirectory(directoryMA);
            }
        }
        monitorDataList = directoryMA.getMonitorDataList();
        monitorManagerOrganization.setMonitorDataDirectory(directoryMA);
        ArrayList<MonitorData> list = monitorManagerOrganization.getMonitorDataDirectory().getMonitorDataList();
        for(int i=0; i<list.size();i++){
            System.out.println(list.get(i).getMonitorName());
        }
    }
    
}
