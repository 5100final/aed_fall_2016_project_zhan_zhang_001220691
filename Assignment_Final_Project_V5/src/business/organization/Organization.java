/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.organization;

import business.monitorData.MonitorDataDirectory;
import business.patientData.PatientDataDirectory;
import business.role.Role;
import business.useraccount.UserAccountDirectory;
import business.workqueue.WorkQueue;
import java.util.ArrayList;

/**
 *
 * @author zml
 */
public abstract class Organization {
    private String name;
    private WorkQueue workQueue;
    private UserAccountDirectory userAccountDirectory;
    private int organizationID;
    private static int counter;
    private MonitorDataDirectory monitorDataDirectory;
    private PatientDataDirectory patientDataDirectory;
    
    public enum Type{
        Admin("Admin Organization"),
        Researcher("Researcher Organization"),
        MedicalDepartmentManager("Medical Department Manager Organization"),
        EnvironmentDepartmentManager("Environment Department Manager Organization"),
        FactoryManager("Factory Manager Organization"),
        MonitoringStationManager("Monitoring Station Manager Organization"),
        MonitoringStationEmployee("Monitoring Station Employee Organization");
        
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }
    
    public Organization(String name) {
        this.name = name;
        workQueue = new WorkQueue();
        userAccountDirectory = new UserAccountDirectory();
        organizationID = counter;
        ++counter;
    }
        
    public abstract ArrayList<Role> getSupportedRole();
    
    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    
    public int getOrganizationID() {
        return organizationID;
    }
    
    public String getName() {
        return name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    public MonitorDataDirectory getMonitorDataDirectory() {
        return monitorDataDirectory;
    }

    public void setMonitorDataDirectory(MonitorDataDirectory monitorDataDirectory) {
        this.monitorDataDirectory = monitorDataDirectory;
    }

    public PatientDataDirectory getPatientDataDirectory() {
        return patientDataDirectory;
    }

    public void setPatientDataDirectory(PatientDataDirectory patientDataDirectory) {
        this.patientDataDirectory = patientDataDirectory;
    }
    

    @Override
    public String toString() {
        return name;
    }
}
