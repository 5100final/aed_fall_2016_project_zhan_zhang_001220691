/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.organization;

import business.role.FactoryManagerRole;
import business.role.Role;
import java.util.ArrayList;

/**
 *
 * @author yanyu
 */
public class FactoryManagerOrganization extends Organization{

    private MonitorManagerOrganization monitorManagerOrganization;
    public FactoryManagerOrganization(){
        super(Type.FactoryManager.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList = new ArrayList<Role>();
        roleList.add(new FactoryManagerRole());
        return roleList;       
    }

    public MonitorManagerOrganization getMonitorManagerOrganization() {
        return monitorManagerOrganization;
    }

    public void setMonitorManagerOrganization(MonitorManagerOrganization monitorManagerOrganization) {
        this.monitorManagerOrganization = monitorManagerOrganization;
    }
    
}
