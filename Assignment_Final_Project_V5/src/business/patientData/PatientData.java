/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.patientData;

/**
 *
 * @author apple
 */
public class PatientData {
    private int year;
    private int month;
    private int totalNum;
    private int dyspneaNum;
    private int arrhythmiaNum;
    private int asthmaNum;
    private int bronchitisNum;
    private String state;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public int getDyspneaNum() {
        return dyspneaNum;
    }

    public void setDyspneaNum(int dyspneaNum) {
        this.dyspneaNum = dyspneaNum;
    }

    public int getArrhythmiaNum() {
        return arrhythmiaNum;
    }

    public void setArrhythmiaNum(int arrhythmiaNum) {
        this.arrhythmiaNum = arrhythmiaNum;
    }

    public int getAsthmaNum() {
        return asthmaNum;
    }

    public void setAsthmaNum(int asthmaNum) {
        this.asthmaNum = asthmaNum;
    }

    public int getBronchitisNum() {
        return bronchitisNum;
    }

    public void setBronchitisNum(int bronchitisNum) {
        this.bronchitisNum = bronchitisNum;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    
    
    
    
}
