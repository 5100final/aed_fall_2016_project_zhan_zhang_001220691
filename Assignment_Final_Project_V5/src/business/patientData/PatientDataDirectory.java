/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.patientData;

import java.util.ArrayList;

/**
 *
 * @author apple
 */
public class PatientDataDirectory {
    private ArrayList<PatientData> patientDataDirectory;
    
    public PatientDataDirectory(){
        patientDataDirectory = new ArrayList<>();
    }

    public ArrayList<PatientData> getPatientDataDirectory() {
        return patientDataDirectory;
    }

    public void setPatientDataDirectory(ArrayList<PatientData> patientDataDirectory) {
        this.patientDataDirectory = patientDataDirectory;
    }
    public PatientData CreatePatientData(String state,int year,int month,int totalNum,
            int dyspneaNum,int arrhythmiaNum,int asthmaNum,int bronchitisNum)
    {
        PatientData pd = new PatientData();
        pd.setState(state);
        pd.setYear(year);
        pd.setMonth(month);
        pd.setTotalNum(totalNum);
        pd.setDyspneaNum(dyspneaNum);//huxikunnan
        pd.setArrhythmiaNum(arrhythmiaNum);//xilvbuqi
        pd.setAsthmaNum(asthmaNum);
        pd.setBronchitisNum(bronchitisNum);//zhiqiguanyan
        patientDataDirectory.add(pd);
        return pd;
        
    }
    
    
}
