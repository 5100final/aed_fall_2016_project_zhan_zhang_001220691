/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import business.organization.Organization;
import business.role.Role;
import business.role.GovermentAdminRole;
import business.stategoverment.StateGoverment;
import java.util.ArrayList;

/**
 *
 * @author zml
 */
public class NationalGoverment extends Organization{
    private static NationalGoverment business;
    private ArrayList<StateGoverment> stateList;

    public ArrayList<StateGoverment> getStateList() {
        return stateList;
    }

    public void setStateList(ArrayList<StateGoverment> stateList) {
        this.stateList = stateList;
    }
    
    public static NationalGoverment getInstance(){
        if(business == null){
            business = new NationalGoverment();
        }
        return business;
    }
    
    public StateGoverment createAndAddState(String name){
        StateGoverment state = new StateGoverment(name);
        stateList.add(state);
        return state;
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        //add particular role
        ArrayList<Role> roleList = new ArrayList();
        roleList.add(new GovermentAdminRole());
        return roleList;
    }
    //calling the super class--organization
    //private constructor
    private NationalGoverment(){
        super(null);
        stateList = new ArrayList();
    }
    
    public boolean checkIfUserNameUnique(String userName){
        if(!this.getUserAccountDirectory().checkIfUsernameIsUnique(userName)){
            return false;
        }
        for(StateGoverment state : stateList){
            
        }
        return true;
    }
}
