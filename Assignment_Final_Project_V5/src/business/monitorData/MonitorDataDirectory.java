/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.monitorData;

import business.monitorData.MonitorData;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author yanyu
 */
public class MonitorDataDirectory {
    
    private ArrayList<MonitorData> monitorDataList;
    public MonitorDataDirectory(){
        monitorDataList =new ArrayList<>();
    }

    public ArrayList<MonitorData> getMonitorDataList() {
        return monitorDataList;
    }

    public void setMonitorDataList(ArrayList<MonitorData> monitorDataList) {
        this.monitorDataList = monitorDataList;
    }
    
    public MonitorData CreateMonitorData(String monitorName,int polutionIndex,Date date,int year,int month,int day){
        MonitorData monitorData = new MonitorData();
        monitorData.setMonitorName(monitorName);
        monitorData.setPolutionIndex(polutionIndex);
        monitorData.setDate(date);
        monitorData.setYear(year);
        monitorData.setMonth(month);
        monitorData.setDay(day);
        
        monitorDataList.add(monitorData);
        return monitorData;
    }
}
