/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.stategoverment;

import business.enterprise.EnterpriseDirectory;
import business.organization.Organization;
import business.role.AdminRole;
import business.role.GovermentAdminRole;
import business.role.Role;
import business.role.StateAdminRole;
import java.util.ArrayList;

/**
 *
 * @author zml
 */
public class StateGoverment extends Organization{
    private String name;
    private EnterpriseDirectory enterpriseDirectory;
    
    public StateGoverment(String name){
        super(name);
        enterpriseDirectory = new EnterpriseDirectory();
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EnterpriseDirectory getEnterpriseDirectory() {
        return enterpriseDirectory;
    }
    
        @Override
    public ArrayList<Role> getSupportedRole() {
        //add particular role
        ArrayList<Role> roleList = new ArrayList();
        roleList.add(new StateAdminRole());   //?????????
        return roleList;
    }
    
    @Override
    public String toString(){
        return name;
    }

    
}
