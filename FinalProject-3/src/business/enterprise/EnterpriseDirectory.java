/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.enterprise;

import java.util.ArrayList;

/**
 *
 * @author zml
 */
public class EnterpriseDirectory {
    private ArrayList<Enterprise> enterpriseList;

    public ArrayList<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }

    public void setEnterpriseList(ArrayList<Enterprise> enterpriseList) {
        this.enterpriseList = enterpriseList;
    }
    
    public EnterpriseDirectory(){
        enterpriseList = new ArrayList();
    }
    
    //create enterprise
    public Enterprise createAndAddEnterprise(String name, Enterprise.EnterpriseType type){
        Enterprise enterprise = null;
        if(type ==Enterprise.EnterpriseType.Factory){
            enterprise = new Factory(name);
            enterpriseList.add(enterprise);
        }else if(type ==Enterprise.EnterpriseType.MonitoringStation){
            enterprise = new MonitoringStation(name);
            enterpriseList.add(enterprise);
        }else if(type ==Enterprise.EnterpriseType.ResearchInstitute){
            enterprise = new ResearchInstitute(name);
            enterpriseList.add(enterprise);
        }else if(type ==Enterprise.EnterpriseType.MedicalDepartment){
            enterprise = new MedicalDepartment(name);
            enterpriseList.add(enterprise);
        }else if(type ==Enterprise.EnterpriseType.EnvironmentDepartment){
            enterprise = new EnvironmentDepartment(name);
            enterpriseList.add(enterprise);
        }
        return enterprise;
    }
}
