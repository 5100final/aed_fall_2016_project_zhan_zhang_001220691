/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.enterprise;

import business.role.Role;
import java.util.ArrayList;

/**
 *
 * @author zml
 */
public class MedicalDepartment extends Enterprise{
    
    public MedicalDepartment(String name) {
        super(name, Enterprise.EnterpriseType.MedicalDepartment);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        return null;
    }
    
}
