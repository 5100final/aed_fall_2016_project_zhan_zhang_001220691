/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.role;

import business.NationalGoverment;
import business.enterprise.Enterprise;
import business.organization.Organization;
import business.organization.OrganizationDirectory;
import business.stategoverment.StateGoverment;
import business.useraccount.UserAccount;
import javax.swing.JPanel;
import userinterface.stategovermentadminrole.StateAdminWorkAreaJPanel;

/**
 *
 * @author zml
 */
public class StateAdminRole extends Role{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, StateGoverment state, NationalGoverment system,OrganizationDirectory directory) {
        return new StateAdminWorkAreaJPanel(userProcessContainer, system,state,directory); 
    }
    
}
