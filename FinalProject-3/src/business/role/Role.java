/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.role;

import business.NationalGoverment;
import business.organization.Organization;
import business.enterprise.Enterprise;
import business.organization.OrganizationDirectory;
import business.stategoverment.StateGoverment;
import business.useraccount.UserAccount;
import javax.swing.JPanel;

/**
 *
 * @author zml
 */
public abstract class Role {
    public enum RoleType{
        Admin("Admin"),
        StateAdmin("StateAdmin"),
        Researcher("Researcher"),
        MDManager("Medical Department Manager"),
        MSEmployee("Monitoring Station Employee");
        
        private String value;
        private RoleType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    
    public abstract JPanel createWorkArea(JPanel userProcessContainer, 
            UserAccount account, 
            Organization organization, 
            Enterprise enterprise, 
            StateGoverment state,
            NationalGoverment system,
            OrganizationDirectory directory);

    @Override
    public String toString() {
        return this.getClass().getName();
    }
}
