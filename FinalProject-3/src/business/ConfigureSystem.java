/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;


import static business.enterprise.Enterprise.EnterpriseType.MedicalDepartment;
import business.enterprise.MedicalDepartment;
import business.enterprise.ResearchInstitute;
import business.organization.MDManagerOrganization;
import business.organization.ResearcherOrganization;
import business.role.GovermentAdminRole;
import business.role.MDManagerRole;
import business.role.ResearcherRole;
import business.stategoverment.StateGoverment;
import business.useraccount.UserAccount;

/**
 *
 * @author zml
 */
public class ConfigureSystem {
    public static NationalGoverment configure(){
        
        NationalGoverment system = NationalGoverment.getInstance();
        StateGoverment state = system.createAndAddState("MA");
        state.setName("MA");
        //???????
        UserAccount ua = system.getUserAccountDirectory().createUserAccount("sysadmin", "sysadmin", new GovermentAdminRole());
        
        //ResearchInstitute researchInstitute = new ResearchInstitute("Research Institute");
        //state.getEnterpriseDirectory().getEnterpriseList().add(researchInstitute);
        
        /*ResearcherOrganization researcherOrganization = new ResearcherOrganization();
        researchInstitute.getOrganizationDirectory().getOrganizationList().add(researcherOrganization);
        
        MedicalDepartment medicalDepartment = new MedicalDepartment("Medical Department");
        state.getEnterpriseDirectory().getEnterpriseList().add(medicalDepartment);
        
        MDManagerOrganization mdmo = new MDManagerOrganization();
        medicalDepartment.getOrganizationDirectory().getOrganizationList().add(medicalDepartment);
        
        
        UserAccount account = new UserAccount();
        account.setUsername("r1");
        account.setPassword("r1");
        account.setRole(new ResearcherRole());
        researcherOrganization.getUserAccountDirectory().getUserAccountList().add(account);
        
        UserAccount account3 = new UserAccount();
        account.setUsername("r2");
        account.setPassword("r2");
        account.setRole(new ResearcherRole());
        researcherOrganization.getUserAccountDirectory().getUserAccountList().add(account3);
        
        UserAccount account2 = new UserAccount();
        account.setUsername("m1");
        account.setPassword("m1");
        account.setRole(new MDManagerRole());
        mdmo.getUserAccountDirectory().getUserAccountList().add(account2);*/
        
        return system;
    }
}
