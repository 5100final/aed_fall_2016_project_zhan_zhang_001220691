/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.organization;

import business.monitorData.MonitorDataDirectory;
import business.role.MonitorManagerRole;
import business.role.Role;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author yanyu
 */
public class MonitorManagerOrganization extends Organization{
    private MonitorDataDirectory monitorDataDirectory;
    
    public MonitorManagerOrganization(){
        super(Type.MonitoringStationManager.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList = new ArrayList<Role>();
        roleList.add(new MonitorManagerRole());
        return roleList;
    }

    public MonitorDataDirectory getMonitorDataDirectory() {
        return monitorDataDirectory;
    }

    public void setMonitorDataDirectory(MonitorDataDirectory monitorDataDirectory) {
        this.monitorDataDirectory = monitorDataDirectory;
    }

   

}
