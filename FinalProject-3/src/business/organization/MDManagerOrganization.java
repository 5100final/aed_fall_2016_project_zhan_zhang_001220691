/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.organization;

import business.role.MDManagerRole;
import business.role.Role;
import java.util.ArrayList;

/**
 *
 * @author apple
 */
public class MDManagerOrganization extends Organization{
    
    public MDManagerOrganization() {
        super(Organization.Type.MedicalDepartmentManager.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new MDManagerRole());
        return roles;
    }
    
}
