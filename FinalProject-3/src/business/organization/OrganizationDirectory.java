/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.organization;

import business.organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author zml
 */
public class OrganizationDirectory {
    
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Type type){
        Organization organization = null;
        if (type.getValue().equals(Type.EnvironmentDepartmentManager.getValue())){
            organization = new EnvironmentManagerOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.FactoryManager.getValue())){
            organization = new FactoryManagerOrganization();
            organizationList.add(organization);
        }else if (type.getValue().equals(Type.MedicalDepartmentManager.getValue())){
            organization = new MDManagerOrganization();
            organizationList.add(organization);
        }else if (type.getValue().equals(Type.MonitoringStationManager.getValue())){
            organization = new MonitorManagerOrganization();
            organizationList.add(organization);
        }else if (type.getValue().equals(Type.MonitoringStationEmployee.getValue())){
            organization = new MonitorEmployeeOrganization();
            organizationList.add(organization);
        }
        return organization;
    }
}
