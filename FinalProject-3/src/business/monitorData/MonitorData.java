/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.monitorData;

import java.util.Date;

/**
 *
 * @author yanyu
 */
public class MonitorData {
    private String monitorName;
    private int polutionIndex;
    private Date date;
    
    public MonitorData(){

    }

    public String getMonitorName() {
        return monitorName;
    }

    public void setMonitorName(String monitorName) {
        this.monitorName = monitorName;
    }

    public int getPolutionIndex() {
        return polutionIndex;
    }

    public void setPolutionIndex(int polutionIndex) {
        this.polutionIndex = polutionIndex;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    @Override
    public String toString(){
        return monitorName;
    }
}
